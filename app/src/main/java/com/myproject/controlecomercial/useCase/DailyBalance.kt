package com.myproject.controlecomercial.useCase

import java.time.LocalDateTime

class DailyBalance(
    val date: LocalDateTime = LocalDateTime.now(),
    val value: Double = 1.0
) {
    override fun toString(): String {
        return "{date: $date, value: $value}"
    }
}