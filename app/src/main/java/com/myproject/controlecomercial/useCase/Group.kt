package com.myproject.controlecomercial.useCase

import java.util.*

class Group(
    val name:String = "",
    private val marketingList:List<Marketing> = listOf()
){

    internal fun calculateAllValue(): Double{
        return marketingList.sumByDouble { marketing ->  marketing.calculateAllValue()}
    }

    internal fun filterMarketingByName(query:String): List<Marketing>{
        return marketingList.filter { marketing -> marketing.name.toLowerCase(Locale.ROOT) == query.toLowerCase(Locale.ROOT) }
    }

    override fun toString(): String {
        return "{name: $name, marketingList: $marketingList}"
    }
}