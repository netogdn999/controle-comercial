package com.myproject.controlecomercial.useCase

class Category(
    private val groupList:List<Group> = listOf()
){

    internal fun calculateAllValue(): Double{
        return groupList.sumByDouble { group -> group.calculateAllValue() }
    }

    internal fun filterGroupByName(query:String): List<Group>{
        return groupList.filter { group -> group.name == query }
    }

    override fun toString(): String {
        return "{groupList: $groupList}"
    }
}