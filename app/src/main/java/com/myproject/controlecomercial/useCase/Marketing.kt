package com.myproject.controlecomercial.useCase

import java.time.LocalDateTime

class Marketing(
    val name:String = "",
    private val dailyBalanceList:List<DailyBalance> = listOf()
){

    internal fun calculateAllValue(): Double{
        return dailyBalanceList.sumByDouble { marketing ->  marketing.value}
    }

    internal fun filterDailyBalanceByDate(query:LocalDateTime): List<DailyBalance>{
        return dailyBalanceList.filter { dailyBalance -> dailyBalance.date == query}
    }

    override fun toString(): String {
        return "{name: $name, dailyBalanceList: $dailyBalanceList}"
    }
}