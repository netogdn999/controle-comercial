package com.myproject.controlecomercial.adapter

import androidx.lifecycle.ViewModel
import com.myproject.controlecomercial.useCase.Category

class SaleViewModel(
    private val viewModelRepository: ViewModelRepository
): ViewModel() {

//    init {
//        val viewModelRepository = SaleRepository.getInstance(SaleDatabase.getInstance().saleDao)
//        ViewModelFactory(viewModelRepository)
//    }

    fun getSale() = viewModelRepository.getData() as Category
}