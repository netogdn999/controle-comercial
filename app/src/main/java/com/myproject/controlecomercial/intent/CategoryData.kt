package com.myproject.controlecomercial.intent

data class CategoryData(
    val groupDataList: List<GroupData> = listOf()
) {
    override fun toString(): String {
        return "{groupList: $groupDataList}"
    }
}