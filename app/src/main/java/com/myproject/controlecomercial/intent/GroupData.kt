package com.myproject.controlecomercial.intent

data class GroupData(
    val name: String = "",
    val marketingDataList:List<MarketingData> = listOf()
){
    override fun toString(): String {
        return "{name: $name, marketingList: $marketingDataList}"
    }
}