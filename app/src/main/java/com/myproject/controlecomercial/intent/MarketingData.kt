package com.myproject.controlecomercial.intent

data class MarketingData (
    val name:String = "",
    val value:Double = 0.0
){
    override fun toString(): String {
        return "{name: $name, value: $value}"
    }
}