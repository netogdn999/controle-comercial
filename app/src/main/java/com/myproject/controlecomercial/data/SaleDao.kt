package com.myproject.controlecomercial.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.myproject.controlecomercial.useCase.Category

class SaleDao: DaoInterface {
    private val saleCache = Category()
    private val sale = MutableLiveData<Category>()

    init {
        sale.value = saleCache
    }

    override fun getData() = sale as LiveData<Category>
}