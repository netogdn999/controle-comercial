package com.myproject.controlecomercial.data

import com.myproject.controlecomercial.adapter.ViewModelRepository
import com.myproject.controlecomercial.useCase.Category

class SaleRepository private constructor(
    private val dao: DaoInterface
): ViewModelRepository {

    companion object {
        @Volatile private var instance: SaleRepository? = null

        fun getInstance(dao: DaoInterface) = instance ?: synchronized(this){
            instance ?: SaleRepository(dao).also { instance = it }
        }
    }

    override fun getData() = dao.getData() as Category
}