package com.myproject.controlecomercial.data

class SaleDatabase private constructor() {

    var saleDao = SaleDao()
        private set

    companion object {
        @Volatile private var instance: SaleDatabase? = null

        fun getInstance() = instance ?: synchronized(this){
            instance ?: SaleDatabase().also { instance = it }
        }
    }
}