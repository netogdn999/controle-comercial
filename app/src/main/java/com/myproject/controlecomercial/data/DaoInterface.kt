package com.myproject.controlecomercial.data

interface DaoInterface {
    fun getData(): Any
}