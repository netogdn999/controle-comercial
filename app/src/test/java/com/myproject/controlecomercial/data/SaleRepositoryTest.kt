package com.myproject.controlecomercial.data

import com.myproject.controlecomercial.useCase.Category
import org.junit.Test

import org.junit.Assert.*

class SaleRepositoryTest {

    @Test
    fun assertNOT_Null_Object(){
        // Given
        val saleDaoSpy = SaleDaoSpy()
        val saleRepository = SaleRepository.getInstance(saleDaoSpy)

        // Then
        assertNotNull(saleRepository)
    }

    @Test
    fun assertTrue_getData() {
        // Given
        val saleDaoSpy = SaleDaoSpy()
        val saleRepository = SaleRepository.getInstance(saleDaoSpy)

        // When
        saleRepository.getData()

        // Then
        assertTrue(saleDaoSpy.isGetDataIsCalled)
    }
}

class SaleDaoSpy: DaoInterface{
    var isGetDataIsCalled = false

    override fun getData(): Any {
        isGetDataIsCalled = true
        return Category()
    }

}