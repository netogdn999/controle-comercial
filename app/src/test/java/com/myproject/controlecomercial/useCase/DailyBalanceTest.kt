package com.myproject.controlecomercial.useCase

import org.junit.Test

import org.junit.Assert.*
import java.time.LocalDateTime

class DailyBalanceTest {

    @Test
    fun assertNOT_Null_Object(){
        // Given
        val dailyBalance = DailyBalance()

        // Then
        assertNotNull(dailyBalance)
    }

    @Test
    fun testToString() {
        // Given
        val time = LocalDateTime.now()
        val dailyBalance = DailyBalance(time, 1.0)

        // Then
        assertEquals("{date: $time, value: 1.0}", dailyBalance.toString())
    }
}