package com.myproject.controlecomercial.useCase

import org.junit.Test

import org.junit.Assert.*
import java.time.LocalDateTime

class CategoryTest {

    @Test
    fun assertNOT_Null_Object(){
        // Given
        val category = Category()

        // Then
        assertNotNull(category)
    }

    @Test
    fun test_calculate_All_Value_From_The_Category() {
        // Given
        val time = LocalDateTime.now()
        val dailyBalance = listOf(DailyBalance(time, 1.0))
        val marketingList = listOf(Marketing("Teste", dailyBalance), Marketing("Teste", dailyBalance), Marketing("Teste", dailyBalance))
        val groupList = listOf(Group("Teste", marketingList), Group("Teste", marketingList))
        val category = Category(groupList)

        // Then
        assertEquals(6.0, category.calculateAllValue(), 0.0)
    }

    @Test
    fun test_filter_Group_By_Name_From_The_Category() {
        // Given
        val time = LocalDateTime.now()
        val dailyBalance = listOf(DailyBalance(time, 1.0))
        val marketingList = listOf(Marketing("Teste", dailyBalance), Marketing("Teste", dailyBalance), Marketing("Teste", dailyBalance))
        val groupList = listOf(Group("Teste", marketingList), Group("A", marketingList))
        val category = Category(groupList)

        // When
        val query = "Teste"

        // Then
        assertEquals(1, category.filterGroupByName(query).size)
    }

    @Test
    fun testToString() {
        // Given
        val time = LocalDateTime.now()
        val dailyBalance = listOf(DailyBalance(time, 1.0))
        val marketingList = listOf(Marketing("Teste", dailyBalance))
        val groupList = listOf(Group("Teste", marketingList))
        val category = Category(groupList)

        // Then
        assertEquals("{groupList: [{name: Teste, marketingList: [{name: Teste, dailyBalanceList: [{date: $time, value: 1.0}]}]}]}", category.toString())
    }
}