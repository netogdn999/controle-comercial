package com.myproject.controlecomercial.useCase

import org.junit.Test

import org.junit.Assert.*
import java.time.LocalDateTime

class GroupTest {

    @Test
    fun testAssertNOt_Null_Object(){
        // Given
        val group = Group()

        // Then
        assertNotNull(group)
    }

    @Test
    fun test_calculate_All_Value_From_The_Group() {
        // Given
        val time = LocalDateTime.now()
        val dailyBalance = listOf(DailyBalance(time, 1.0))
        val marketingList = listOf(Marketing("Teste", dailyBalance), Marketing("Teste", dailyBalance), Marketing("Teste", dailyBalance))
        val group = Group("Teste", marketingList)

        // Then
        assertEquals(3.0, group.calculateAllValue(), 0.0)
    }

    @Test
    fun test_filter_Marketing_By_Name_From_The_group() {
        // Given
        val time = LocalDateTime.now()
        val dailyBalance = listOf(DailyBalance(time, 1.0))
        val marketingList = listOf(Marketing("Teste", dailyBalance), Marketing("Teste", dailyBalance), Marketing("A", dailyBalance))
        val group = Group("Teste", marketingList)

        // When
        val query = "Teste"

        // Then
        assertEquals(2, group.filterMarketingByName(query).size)
    }

    @Test
    fun testToString() {
        // Given
        val time = LocalDateTime.now()
        val dailyBalance = listOf(DailyBalance(time, 1.0))
        val marketingList = listOf(Marketing("Teste", dailyBalance))
        val group = Group("Teste", marketingList)

        // Then
        assertEquals("{name: Teste, marketingList: [{name: Teste, dailyBalanceList: [{date: $time, value: 1.0}]}]}", group.toString())
    }
}