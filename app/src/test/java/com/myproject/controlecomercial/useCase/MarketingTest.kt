package com.myproject.controlecomercial.useCase

import org.junit.Test

import org.junit.Assert.*
import java.time.LocalDateTime

class MarketingTest {

    @Test
    fun testAssertNOT_Null_Object(){
        // Given
        val marketing = Marketing()

        // Then
        assertNotNull(marketing)
    }

    @Test
    fun test_calculate_All_Value_From_The_Marketing() {
        // Given
        val time = LocalDateTime.now()
        val dailyBalance = listOf(DailyBalance(time, 1.0), DailyBalance(time, 1.0), DailyBalance(time, 1.0))
        val marketing = Marketing("Teste", dailyBalance)

        // Then
        assertEquals(3.0, marketing.calculateAllValue(), 0.0)
    }

    @Test
    fun test_filter_Daily_Balance_By_Date_From_The_Marketing() {
        // Given
        val time = LocalDateTime.now()
        val dailyBalance = listOf(DailyBalance(time, 1.0), DailyBalance(LocalDateTime.parse("2020-06-28T00:00:00.000"), 1.0), DailyBalance(LocalDateTime.parse("2020-06-28T00:00:00.000"), 1.0))
        val marketing = Marketing("Teste", dailyBalance)

        // When
        val query = LocalDateTime.parse("2020-06-28T00:00:00.000")

        // Then
        assertEquals(2, marketing.filterDailyBalanceByDate(query).size)
    }

    @Test
    fun testToString() {
        // Given
        val time = LocalDateTime.now()
        val dailyBalance = listOf(DailyBalance(time, 1.0))
        val marketing = Marketing("Teste", dailyBalance)

        // Then
        kotlin.test.assertEquals("{name: Teste, dailyBalanceList: [{date: $time, value: 1.0}]}", marketing.toString())
    }
}